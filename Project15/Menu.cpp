#include "Menu.h"

Menu::Menu()
{
	isMenu = 1;
};

void Menu::callMenu(sf::RenderWindow & window)
{
	sf::Image menutab1;
	menutab1.loadFromFile("images/tab1.png");
	menutab1.createMaskFromColor(sf::Color(255, 255, 255));
	sf::Texture menuTexture1, menuBackground;
	menuTexture1.loadFromImage(menutab1);
	menuBackground.loadFromFile("images/menu.jpg");
	sf::Sprite menu1(menuTexture1), menuBg(menuBackground);

	int menuNum = 0;
	menu1.setPosition(100, 30);
	menuBg.setPosition(0, 0);
	while (isMenu)
	{
		sf::Event event;
		menu1.setColor(sf::Color::White);
		menuNum = 0;
		window.clear(sf::Color(0, 0, 0));
		while (window.pollEvent(event))
		{
			if ((event.type == sf::Event::Closed) || (event.key.code == sf::Keyboard::Escape)) window.close();
		}
		if (sf::IntRect(100, 30, 400, 100).contains(sf::Mouse::getPosition(window))) { menu1.setColor(sf::Color::Blue); menuNum = 1; }
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			if (menuNum == 1)isMenu = false;
		}
		window.draw(menuBg);
		window.draw(menu1);
		window.display();
	}
};
