#pragma once
#include <SFML/Graphics.hpp>

class Posts
{
	sf::Text text_;
	sf::Font font_;

public:
	Posts(const sf::String &myFont, int fontSize, const sf::String &text, int posX, int posY);

	sf::Text getText();
};