#pragma once
#include <SFML/Graphics.hpp>
#include "GameObjects.h"
#include "Constants.h"


struct GameFunctions {

	static void chekBall(GameObject &ball, GameObject &wood, GameObject &board);

	static void replay(GameObject &board, GameObject &Ball, std::vector <GameObject> &woods);
	   
	static void directionBall(GameObject &ball, GameObject &board, double time,bool &lose);
};
