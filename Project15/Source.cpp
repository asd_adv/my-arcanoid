﻿#include <SFML/Graphics.hpp>
#include <iostream> 
#include <vector>
#include <string>
#include "GameObjects.h"
#include "Posts.h"
#include "Menu.h"
#include "GameFunctions.h"
#include "Constants.h"

using namespace sf;

int main()
{
	Menu menu;
	Clock gameTime;
	Clock pauseTime;

	Posts messageGameOver("1.ttf", 100, "game over", 299, 99);
	Posts messagePause("1.ttf", 50, "Game paused, press key 'space'", 140, 299);
	Posts messagePreesR("1.ttf", 50, "press key 'R' for new game", 209, 249);

	RenderWindow window(VideoMode(1024, 768), "crash box");
	menu.callMenu(window);
	Texture bg;
	bg.loadFromFile("images/bg.png");
	Sprite bgsprite;
	bgsprite.setTexture(bg);

	const int woodsQty = 18;
	bool lose = 0;
	bool replay = 0;
	bool pause = 0;
	int gameSpeed=750;// !!! game speed 750 standart
	std::vector <GameObject> arrWoods;
	arrWoods.reserve(woodsQty);
	
	for (int i = 0; i < woodsQty; i++)
	{
		int b = 0;
		if (i > 8)b = 1;
		int a = i % 9;
		arrWoods.push_back(GameObject(70 + (100 * a), 90 + (40 * b), 80, 30, 80, 70,0.3,0.3, "board_and_tools.png"));
	}
	 
	GameObject myBoard(450, 650, 150, 20, 40, 40, 0.35,0.35, "board_and_tools.png");
	GameObject ball(480, 250, 21, 21, 40, 70, 0.3,0.3, "board_and_tools.png");
	
	ball.changeDirectionX(1);
	ball.changeDirectionY(1);

	while (window.isOpen())
	{
		Event event;

		sf::Time elapsed1 = pauseTime.getElapsedTime();
		double pauseSec = elapsed1.asSeconds();

		double time = gameTime.getElapsedTime().asMicroseconds();
		gameTime.restart();
		time = time / gameSpeed;

		
		while (window.pollEvent(event))
		{
			if ((event.type == Event::Closed) || (event.key.code == Keyboard::Escape))
			{
				window.close();
			}
		}

		if ((Keyboard::isKeyPressed(Keyboard::R)) && (lose))
		{
			replay = true;
		}

		if ((Keyboard::isKeyPressed(Keyboard::Space)) && (pauseSec > 0.2))
		{
			pause = !pause;
			pauseTime.restart();
		}

		if ((!lose) && (pause)) {

			if (((Keyboard::isKeyPressed(Keyboard::Left)) || (Keyboard::isKeyPressed(Keyboard::A))) && (myBoard.getX() > 61))
			{
				if (ball.getY() >= BOT_BORDER)//boost speed ball 
				{
					ball.setAccelerationX(1.2);
					ball.setAccelerationY(1.2);
				}
				myBoard.changeDirectionX(-1);
				myBoard.upDateX(time);
			}

			if (((Keyboard::isKeyPressed(Keyboard::Right)) || (Keyboard::isKeyPressed(Keyboard::D))) && (myBoard.getX() < 810))
			{
				if (ball.getY() >= BOT_BORDER)//boost speed ball 
				{
					ball.setAccelerationX(1.2);
					ball.setAccelerationY(1.2);
				}
				myBoard.changeDirectionX(1);
				myBoard.upDateX(time);
			}			
		}
		//myBoard.setSpritePos();

		if (ball.getY() <= 200)//stop boost
		{
			ball.setAccelerationX(1);
			ball.setAccelerationY(1);
		}

		if ((!lose) && (pause))
		{
			GameFunctions::directionBall(ball, myBoard, time, lose);
			ball.upDateXY(time);
		}
		//ball.setSpritePos();
		
		for (auto && wood : arrWoods)
		{
			GameFunctions::chekBall(ball, wood,myBoard);
		}

		window.clear();
		window.draw(bgsprite);
		if (!lose)
		{
			for (auto && wood : arrWoods)
			{
				if (wood.getLifeStatus()) wood.draw(window);  
			}
			ball.setSpritePos();//
			ball.draw(window);

			myBoard.setSpritePos();//
			myBoard.draw(window);
		}

		if ((!pause)&& (!lose))
		{
			window.draw(messagePause.getText());
		}

		if (lose)
		{
			window.draw(messagePreesR.getText());
			window.draw(messageGameOver.getText());
		}

		if (replay)
		{
			GameFunctions::replay(myBoard, ball, arrWoods);
			lose = 0;
			pause = 0;
			replay = !replay;
		}

		window.display();
	}
}