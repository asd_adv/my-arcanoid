//game obj.cpp
#include "GameObjects.h"


GameObject::GameObject(double X, double Y, double W, double H, double ix, double iy, double speedX, double speedY, const sf::String &f)
	:x_(X), y_(Y), width_(W), height_(H), speedX_(speedX), speedY_(speedX), file_(f), life_(true), accelerationX_(1), accelerationY_(1)
{	
	image_.loadFromFile("images/" + file_);
	texture_.loadFromImage(image_);
	sprite_.setTexture(texture_);
	sprite_.setPosition(x_, y_);
	sprite_.setTextureRect(sf::IntRect(ix, iy, width_, height_));
} 

void GameObject::upDateX(double time)
{
	x_ += time*(speedX_*accelerationX_);
	//sprite_.setPosition(x_, y_);
}

void GameObject::upDateXY(double time)
{
	x_ +=time * (speedX_*accelerationX_);
	y_ +=time * (speedY_*accelerationY_);
}


void GameObject::draw(sf::RenderWindow &window)
{
	window.draw(sprite_);
}

void GameObject::changeDirectionX(int dir)
{
	if (((dir < 0) && (speedX_ > 0))|| ((dir > 0) && (speedX_ < 0)))
	{
		speedX_ *= -1;
	}
}


void GameObject::changeDirectionY(int dir)
{
	if (((dir < 0) && (speedY_ > 0)) || ((dir > 0) && (speedY_ < 0)))
	{
		speedY_ *= -1;
	}
}


void GameObject::setAccelerationX(double aX)
{
	accelerationX_ = aX;
};

void GameObject::setAccelerationY(double aY)
{
	accelerationY_ = aY;
};


void GameObject::setSpritePos()
{
	sprite_.setPosition(x_, y_);
}

void GameObject::setLifeStatus(bool a) 
{
	life_ = a;
}

void GameObject::setPositionX(double x)
{
	x_ = x;
}

void GameObject::setPositionY( double y)
{
	y_ = y;
}


double GameObject::getWidth()
{
	return width_;
}

double GameObject::getHeight()
{
	return height_;
}

double GameObject::getX()
{
	return x_;
}

double GameObject::getY()
{
	return y_;
}

bool GameObject::getLifeStatus()
{
	return life_;
}