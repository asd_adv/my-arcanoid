//game obj.h
#pragma once
#include <SFML/Graphics.hpp>

class GameObject
{

	double x_;
	double  y_;
	double speedX_; 
	double speedY_;
	double accelerationX_ ;
	double accelerationY_ ;
	double width_; 
	double height_;

	sf::String file_;
	sf::Image image_;
	sf::Texture texture_;
	sf::Sprite sprite_;
	bool life_;
	public:


	GameObject(double X, double Y, double W, double H, double ix, double iy, double speedX, double speedY, const sf::String &f);



	void draw(sf::RenderWindow &window);
	
	void upDateX(double time);

	void upDateXY(double time);


	void changeDirectionX(int dir);

	void changeDirectionY(int dir);

	   
	void setPositionY(double y);

	void setPositionX(double x);

	void setSpritePos();
	
	void setLifeStatus(bool a);

	void setAccelerationX(double a);

	void setAccelerationY(double a);


	double getX();

	double getY();

	bool getLifeStatus();

	double getWidth();

	double getHeight();
};





