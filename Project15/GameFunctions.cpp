#include "GameFunctions.h"

void GameFunctions::chekBall(GameObject &ball, GameObject &wood, GameObject &board)//a=ball,b=wood
{
	if (wood.getLifeStatus() &&
		(((((ball.getX() >= wood.getX()) && (ball.getX() <= wood.getX() + wood.getWidth())) && ((wood.getY() <= ball.getY()) && (wood.getY() + wood.getHeight() >= ball.getY())))) ||
		((((ball.getX() + ball.getHeight() >= wood.getX()) && (ball.getX() + ball.getHeight() <= wood.getX() + wood.getWidth())) && ((wood.getY() <= ball.getY()) && (wood.getY() + wood.getHeight() >= ball.getY())))) ||
		((((ball.getX() + ball.getHeight() >= wood.getX()) && (ball.getX() + ball.getHeight() <= wood.getX() + wood.getWidth())) && ((wood.getY() <= ball.getY() + ball.getHeight()) && (wood.getY() + wood.getHeight() >= ball.getY() + ball.getHeight())))) ||
		((((ball.getX() >= wood.getX()) && (ball.getX() <= wood.getX() + wood.getWidth())) && ((wood.getY() <= ball.getY() + ball.getHeight()) && (wood.getY() + wood.getHeight() >= ball.getY() + ball.getHeight()))))))
	{
		wood.setLifeStatus(0);
		ball.changeDirectionX(-1);
		ball.changeDirectionY(1);
	}
};


void GameFunctions::replay(GameObject &board, GameObject &Ball, std::vector <GameObject> &woods)
{

	for (size_t i = 0; i < woods.size(); i++)
	{
		woods[i].setLifeStatus(1);
	}
	board.setPositionX(450);
	Ball.setPositionX(450);
	Ball.setPositionY(250);
}


void GameFunctions::directionBall(GameObject &ball, GameObject &board, double time, bool &lose)
{
	if (ball.getX() >= RIGHT_BORDER)ball.changeDirectionX(-1);
	if (ball.getX() <= LEFT_BORDER)ball.changeDirectionX(1);
	if (ball.getY() <= TOP_BORDER)ball.changeDirectionY(1);
	if (ball.getY() >= BOT_BORDER)
	{
		if (((board.getX() - ball.getWidth()) > ball.getX()) || (board.getX() + board.getWidth()) < (ball.getX()))
		{
			lose = 1;
		}
		else
		{
			ball.changeDirectionY(-1);
			lose = 0;
		}
	}
}
