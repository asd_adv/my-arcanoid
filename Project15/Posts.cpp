#include "Posts.h"


Posts::Posts(const sf::String &myFont, int fontSize, const sf::String &text, int posX, int posY)
	{
		font_.loadFromFile(myFont);
		text_ = sf::Text("", font_, fontSize);
		text_.setStyle(sf::Text::Bold);
		text_.setString(text);
		text_.setPosition(posX, posY);
	}

	sf::Text Posts::getText()
	{
		return(text_);
	}
