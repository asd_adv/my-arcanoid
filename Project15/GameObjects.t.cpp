#include <gtest/gtest.h>
#include <GameObjects.h>

// See: https://github.com/google/googletest/blob/master/googletest/docs/primer.md


TEST(TestGameObject, CheckMoving)
{
    GameObject obj(0,0,0,0,0,0,0,0,"Test");

    ASSERT_EQ(obj.getX(), 0.0);
    ASSERT_EQ(obj.getY(), 0.0);
}
